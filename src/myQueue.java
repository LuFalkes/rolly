import java.util.Iterator;

/**
 * * Created by lucas ouellette-falkenstein,
 * PArt of Rolly expert system, for professor frank seidel
 * assignment 5
 * A very very basic queue. Here for reasons I can't recall.
 * TODO, start writing your ideas down.
 *
 */
public class myQueue<T> {

    private TwoWayLinkedList<T> linkedList =  new TwoWayLinkedList<T>();

    public void enqueue (T t){
        linkedList.add(t);
    }

    public T dequeue(){
        return linkedList.removeFirst();
    }

    public int size(){
        return linkedList.size();
    }

    public boolean isEmpty() {
        return linkedList.size()==0; //eval as bool
    }

    public boolean contains(Object o) {
        return linkedList.contains(o);
    }

    public Iterator iterator() {
        return linkedList.listIterator();
    }

    public Object[] toArray() {
        return linkedList.toArray();
    }

    @Override
    public String toString() {
        return "myQueue{" +
                       "linkedList=" + linkedList +
                       '}';
    }
}
