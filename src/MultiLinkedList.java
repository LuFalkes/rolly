import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by lucas ouellette-falkenstein,
 * PArt of Rolly expert system, for professor frank seidel
 * assignment 5
 */
public class MultiLinkedList<T extends Comparable<T>> extends AbstractSet  {

    private ArrayList<Node> nodes = new ArrayList<>();//todo swap back to AVLTree?

    public MultiLinkedList() {}
    public MultiLinkedList(T primeElement, Collection<T> elements) {
        add(primeElement, elements);
    }

    @SuppressWarnings("NullableProblems")
    public Object[] toArray() {
        return nodes.parallelStream()
                    .map(x -> x.element)
                    .toArray();
    }

//    public boolean retainAll(Collection c) {
//        return nodes.retainAll(c);
//    }
//
//    public boolean containsAll(Collection c) {
//        return nodes.containsAll(c);
//    }//TODO implement the above methods such that they look at node elements not nodes data structure

    public void clear(){
        nodes.clear();
    }

    public int size() {
        return nodes.size();
    }

    public boolean isEmpty() {
        return nodes.size() > 0;
    }

    public boolean contains(Object o) {
        return nodes.stream().filter(node -> node.element.equals(o)).findAny().isPresent();//this here is
        // sometimes causing null pointer exceptions for no reason. Need to test more, but since it only happens
        // on occasion it must be threading related... it evens happens after switching from parallel stream to stream
    }

    public Stream parallelStream() {
        return nodes.parallelStream();
    }

    public Stream stream() {
        return nodes.stream();
    }

    @SuppressWarnings("NullableProblems")
    public java.util.Iterator iterator() {
        return nodes.iterator();
    }

    public boolean add(T element, Collection<T> elementAssociations){
        if (this.contains(element)) {//if the node we are working with already exists and so we are using an old prime node...
            final Node primeNode = nodes.parallelStream()
                                        .filter(aNode -> aNode.element.equals(element))
                                        .findFirst()
                                        .get();//get the single node to be our prime node

            ArrayList<T> workingCollection = new ArrayList<>(elementAssociations);
            for (T t : elementAssociations){//TODO improve the time complexity here from n^2, likely with a tree
                nodes.parallelStream()
                     .filter(elemNode -> elemNode.element.equals(t))
                     .forEach(elemNode -> {//if the node we need exists, modify it
                         elemNode.addAssociation(primeNode);
                         primeNode.addAssociation(elemNode);
                         workingCollection.remove(t);//remove the item from our list so the remaining the items will
                         // need a new node
                     });
            }

            workingCollection.parallelStream()
                             .map(Node::new)
                             .forEach(node -> {
                                 node.addAssociation(primeNode);
                                 primeNode.addAssociation(node);
                                 nodes.add(node);
                             });

            elementAssociations.parallelStream()
                               .filter(identity -> !this.contains(identity))
                               .map(Node::new)
                               .forEach(streamNode -> {
                                   streamNode.addAssociation(primeNode);
                                   primeNode.addAssociation(streamNode);
                                   nodes.add(streamNode);
                               });

        } else {
            //if there is no such existing element for the prime node, we make new ones
            final Node primeNode = new Node(element);

            elementAssociations.parallelStream()
                               .filter(identity -> !this.contains(identity))
                               .map(Node::new)
                               .forEach(node -> {
                                   node.addAssociation(primeNode);
                                   primeNode.addAssociation(node);
                                   nodes.add(node);
                               });
            nodes.add(primeNode);
        }
        return true;
    }
    /**I was very hesitant to expose the guts of this class, for some reason it just bugged me. But I find myself in
     * a position where I need to access the collection of associations in each node,so ...*/
    public ArrayList<Node> getNodes() {
        return nodes;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public String toString() {
       return "MultiLinkedList{" + "nodes=" + nodes.toString() + '}';
    }

    class Node implements Comparable<Node> {
        final ArrayList<Node> associations = new ArrayList<>();
        T element;

        public Node(T t){
            element = t;
        }

        public String associationsToString(){/**helper method to stop infinite loop of 'append -> valueof -> node
         .toString -> + operator starts append' by providing alternate toString*/
            final StringBuilder returnString = new StringBuilder();
            associations.parallelStream()
                        .map(node -> node.element)
                        .forEach(item -> returnString.append(item).append(","));
            return returnString.toString();
        }

        public void addAssociation(Node o){
            if (!(associations.contains(o))){
                associations.add(o);
            }
        }

        /**
         * Returns a list of the association values, of type T. Used mainly for the main class so that I can use
         * Collections.disjoint() on the node.associations list despite the fact that it is a list of nodes and not a
         * list of strings
         */
        public List<T> associationsToType() {
            return associations.parallelStream()
                               .map(node -> node.element)
                               .collect(Collectors.toList());
        }

        public boolean contains(T t){
            return associations.contains(t);
        }


        @Override
        public String toString() {
            return "Node{" +
                           "element=" + element +
                           ", associations=" + associationsToString() +
                           "}\n";
        }

        @Override
        public int compareTo(Node o) {
            if (this.element.compareTo(o.element)>0){
                return 10;
            } else if (this.element.compareTo(o.element)<0){
                return -10;
            } else {
                return 0;
            }
        }
    }
}
