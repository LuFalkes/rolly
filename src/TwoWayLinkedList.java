import java.util.AbstractSequentialList;
import java.util.ListIterator;

/**
 * * Created by lucas ouellette-falkenstein,
 * PArt of Rolly expert system, for professor frank seidel
 * assignment 5
 */
@SuppressWarnings("ALL")
public class TwoWayLinkedList <E> extends AbstractSequentialList<E> {
    private Node<E> head, tail;//head is the first item in the list, tail is the last
    private int size;

    public TwoWayLinkedList(){
    }

    public TwoWayLinkedList(E[] things){
        for (E thing : things) {
            add(thing);
        }
    }

    public E getFirst(){
        if (size==0){
            return null;
        } else {
            return head.element;
        }
    }

    public E getLast(){
        if (size ==0){
            return null;
        } else {
            return tail.element;
        }
    }

    public void addFirst(E e) {
        Node<E> newNode = new Node<>(e);
        newNode.next = head;
        head = newNode;
        size++;

        if (tail == null) {
            tail = head;
        }
    }

    public void addLast(E e) {
        Node<E> newNode = new Node<>(e); // Create a new for element e

        if (tail == null) {
            head = tail = newNode; // The new node is the only node in list
        } else {
            tail.next = newNode; // Link the new with the last node
            tail = tail.next; // tail now points to the last node
        }
        size++;
    }
    //was going to add an addAll method but the superclass already has a nice one
    @SuppressWarnings("NullableProblems")
    @Override
    public void add(int index, @SuppressWarnings("NullableProblems") E e) {
        if (index == 0) {
            addFirst(e);
        } else if (index >= size) {
            addLast(e);
        } else {
            Node<E> current = head;
            for (int i = 1; i < index; i++) {
                current = current.next;
            }
            Node<E> temp = current.next;
            current.next = new Node<>(e);
            (current.next).next = temp;
            size++;
        }
    }

    public E removeFirst() {
        if (size == 0) {
            return null;
        } else {
            Node<E> temp = head;
            head = head.next;
            size--;
            if (head == null) {
                tail = null;
            }
            return temp.element;
        }
    }

    public E removeLast() {
        if (size == 0) {
            return null;
        } else if (size == 1) {
            Node<E> temp = head;
            head = tail = null;
            size = 0;
            return temp.element;
        } else {
            Node<E> current = head;

            for (int i = 0; i < size - 2; i++) {
                current = current.next;
            }

            Node<E> temp = tail;
            tail = current;
            tail.next = null;
            size--;
            return temp.element;
        }
    }
    @Override
    public E remove(int index) {
        if (index < 0 || index >= size) {
            return null;
        } else if (index == 0) {
            return removeFirst();
        } else if (index == size - 1) {
            return removeLast();
        } else {
            Node<E> previous = head;

            for (int i = 1; i < index; i++) {
                previous = previous.next;
            }

            Node<E> current = previous.next;
            previous.next = current.next;
            size--;
            return current.element;
        }
    }

    @Override
    public void clear() {
        size = 0;
        head = tail = null;
    } //technically this would work, but relies heavily on
    //garbage collection...

//    @Override
//    public boolean contains(E e) {
//        return super.contains(e); //Call method in AbstractCollection
//    }
//
//    @Override
//    public int indexOf(E e) {
//        return super.indexOf(e); //AbstractList method
//    }
//
//    @Override
//    public int lastIndexOf(E e) {
//        return super.lastIndexOf(e);
//    }
//TODO, get the above block to not fon
    /**
     *  Type erasure is giving me grief about these methods for clashing with the super method
    they call, so I'm not implementing it. I understand the assignment says to implement all method
    inside the MyLinkedList but...Well they should be inherited in anyway. Right?
     */

    @Override
    public E set(int index, E e){
        return super.set(index, e);
    }

    @Override
    public E get(int index) {
        return super.get(index);
    }
    //TODO, change set and get methods from superclass method to my own implementation. Or maybe not?
    @Override
    public int size() {
        return size;
    }

//    @Override
//    public void sort(Comparator<? super E> c) {
//      //Make this a quicksort that reverts to heapsort under certain circumstances?
//    }
//
//    @Override
//    public Stream<E> stream() {
//        return null;
//    }
//
//    @Override
//    public void forEach(Consumer<? super E> action) {
//
//    }

//TODO, if I have time implement the above methods, then I could use this in the proj

    @Override
    public java.util.Iterator<E> iterator() {
        return new LinkedListIterator();
    } // We have no plans to use this with a set at any point so it is up for removal

    @Override
    public java.util.ListIterator<E> listIterator(){
        return listIterator(0);
    }

    @Override
    public java.util.ListIterator<E> listIterator(int index) {
        checkIndex(index);
        return new LinkedListIterator(index);
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");

        Node<E> current = head;
        for (int i = 0; i < size; i++) {
            assert current != null;//assertions are a very functional way of doing things, at least it seems so
            result.append(current.element);
            current = current.next;
            if (current != null) {
                result.append(", ");
            }
            else {
                result.append("]");
            }
        }
        return result.toString();
    }

    private static class Node<E> {
        E element;
        Node<E> next;
        Node<E> previous;

        public Node(E e){
            element = e;
        }

    }

    private class LinkedListIterator implements ListIterator<E> {
        //near complete redo of this class from what was in text
        //cursor is current...right?
        //this is where every bug i've had come from, and I'm still fuzzy about it's behavior
        private Node<E> current = head;
        private Node<E> lastAccessed = null; //because stupid set method

        public LinkedListIterator(){//no arg constructor defaults to head as current
        }

        public LinkedListIterator(int index){
            for (int i = 0; i < index; i++){
                //move up till the relevant index to start, so that when we return in listIterator it returns in the
                //desired position
                next();
            }
        }

        @Override
        public boolean hasNext() {
            return (current.next != null); //changed this from 'tail' to 'null' TODO check that worked
        }

        @Override
        public E next() {
            lastAccessed = current;
            if (!hasNext()){
                return null;
            }
            current = current.next; //Swapped this line
            return current.element; //with this line. TODO, see if that works
        }

        @Override
        public boolean hasPrevious() {
            return (current.previous) != null;
            //Changed this from 'head' to 'null' TODO check that worked
        }

        @Override
        public E previous() {
            if (!hasPrevious()){
                return null;
            }
            current = current.previous;
            lastAccessed = current;
            return current.element;
        }

        @Override
        public int nextIndex() {
            return indexOf(current.next);
        }

        @Override
        public int previousIndex() {
            return indexOf(current.previous);
        }

        @Override
        public void remove() {
            if (current == null){
                throw new IllegalStateException();
            }
            Node<E> newPrev = current.previous;
            Node<E> newNext = current.next;
            newPrev.next = newNext;
            newNext.previous = newPrev;
            size--;
            current = newNext;
        }

        @Override
        public void set(E e) {//screw it, will never need this
            if (lastAccessed == null){
                throw new IllegalStateException();
            }
            lastAccessed.element = e;
        }

        @Override
        public void add(E e) {//just toss exceptions now, need to be able to run test cases on something at least
            Node<E> newPrev = current.previous;
            Node<E> newNode = new Node<>(e);
            Node<E> newNext = current;
            newPrev.next = newNode;
            newNode.next = newNext;
            newNext.previous = newNode;
            newNode.previous = newPrev;
            size++;
        }
    }
}
