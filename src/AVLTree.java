import java.util.ArrayList;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *
 * Created by lucas ouellette-falkenstein,
 * PArt of Rolly expert system, for professor frank seidel
 * assignment 5
 *
 *
 * AVL tree class based somewhat off the one in the book. I inlined the relevant BST superclass though, saw no reason
 * not to since I do not intend to deal with BST's directly. also scrapped the abstract class and integrated it into
 * the Tree interface as default methods. I'm using java 8 anyway, might as well start getting into some of the goodies
 *
 * Hopefully this will be more efficient as a nodes data store than an arraylist. Maybe not as good for iteration.
 * but...well we'll see
 *
 * way is to have it implement Collection which drags in a bunch of contract methods that demand I do the type
 * erasure dance, and I am not doing that, there must be a more elegant way.
 * */



public class AVLTree<E extends Comparable<E>> implements Iterable<E>{
    AVLTreeNode<E> root;
    int size = 0;

    /** default constructor */
    public AVLTree() {
    }

    /** Create an AVL tree from an array of objects */
    public AVLTree(E[] objects) {
        for (E object : objects){
            add(object);
        }
    }

    /** Insert an element and rebalance if necessary */
    public boolean add(E e) {
        if (!attemptAdd(e)){
            return false;
        } else {
            balancePath(e);
        }

        return true; // e is inserted
    }

    /**helper method made in the mess of inlining things. Will re order/inline later*/
    public boolean attemptAdd(E e){
        if (root == null) {
            root = new AVLTreeNode<>(e); // Create a new root
        }
        else {
            // find parent node
            AVLTreeNode<E> parent = null;
            AVLTreeNode<E> current = root;
            while (current != null)
                if (e.compareTo(current.element) < 0) {
                    parent = current;
                    current = current.left;
                }
                else if (e.compareTo(current.element) > 0) {
                    parent = current;
                    current = current.right;
                }
                else {
                    return false; // Duplicate node not inserted
                }

            // Create the new node and attach it to the parent node
            if (e.compareTo(parent.element) < 0) {
                parent.left= new AVLTreeNode<>(e);
            }
            else {
                parent.right = new AVLTreeNode<>(e);
            }
        }

        size++;
        return true; // Element inserted successfully
    }

    /** Update the height of a node */
    private void updateHeight(AVLTreeNode<E> node) {
        if (node.left == null && node.right == null) // node is a leaf
        {
            node.height = 0;
        }
        else {
            if (node.left == null) // node has no left subtree
            {
                node.height = 1 + node.right.height;
            } else {
                if (node.right == null) // node has no right subtree
                {
                    node.height = 1 + node.left.height;
                } else {
                    node.height = 1 + Math.max(node.right.height,
                                               node.left.height);
                }
            }
        }
    }

    
    /** Returns a path from the root leading to the specified element */
    public java.util.ArrayList<AVLTreeNode<E>> getPathOf(E e) {
        java.util.ArrayList<AVLTreeNode<E>> list = new java.util.ArrayList<>();
        AVLTreeNode<E> current = root; // Start from the root

        while (current != null) {
            list.add(current); // Add the node to the list
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else {
                break;
            }
        }

        return list; // Return an array list of nodes
    }

     /** Get the number of nodes in the tree */
    public int size() {
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    /** Balance the nodes in the path from the specified
     * node to the root if necessary
     */
    private void balancePath(E e) {
        java.util.ArrayList<AVLTreeNode<E>> path = getPathOf(e);
        for (int i = path.size() - 1; i >= 0; i--) {
            AVLTreeNode<E> A = path.get(i);
            updateHeight(A);
            AVLTreeNode<E> parentOfA = (A == root) ? null : path.get(i - 1);

            switch (balanceFactor(A)) {
                case -2:
                    if (balanceFactor(A.left) <= 0) {
                        balanceLL(A, parentOfA); // Perform LL rotation
                    }
                    else {
                        balanceLR(A, parentOfA); // Perform LR rotation
                    }
                    break;
                case +2:
                    if (balanceFactor(A.right) >= 0) {
                        balanceRR(A, parentOfA); // Perform RR rotation
                    }
                    else {
                        balanceRL(A, parentOfA); // Perform RL rotation
                    }
            }
        }
    }

    /** Return the balance factor of the node */
    private int balanceFactor(AVLTreeNode<E> node) {
        if (node.right == null) // node has no right subtree
        {
            return -node.height;
        }
        else if (node.left == null) // node has no left subtree
        {
            return +node.height;
        }
        else {
            return node.right.height - node.left.height;
        }
    }

    /** Balance LL (see Figure 27.1) */
    private void balanceLL(AVLTreeNode<E> A, AVLTreeNode<E> parentOfA) {
        AVLTreeNode<E> B = A.left; // A is left-heavy and B is left-heavy

        if (A == root) {
            root = B;
        }
        else {
            if (parentOfA.left == A) {
                parentOfA.left = B;
            }
            else {
                parentOfA.right = B;
            }
        }

        A.left = B.right; // Make T2 the left subtree of A
        B.right = A; // Make A the left child of B
        updateHeight(A);
        updateHeight(B);
    }

    private void balanceLR(AVLTreeNode<E> A, AVLTreeNode<E> parentOfA) {
        AVLTreeNode<E> B = A.left;
        AVLTreeNode<E> C = B.right;

        if (A == root) {
            root = C;
        }
        else {
            if (parentOfA.left == A) {
                parentOfA.left = C;
            }
            else {
                parentOfA.right = C;
            }
        }

        A.left = C.right; // Make T3 the left subtree of A
        B.right = C.left; // Make T2 the right subtree of B
        C.left = B;
        C.right = A;

        // Adjust heights
        updateHeight(A);
        updateHeight(B);
        updateHeight(C);
    }

    private void balanceRR(AVLTreeNode<E> A, AVLTreeNode<E> parentOfA) {
        AVLTreeNode<E> B = A.right; // A is right-heavy and B is right-heavy

        if (A == root) {
            root = B;
        }
        else {
            if (parentOfA.left == A) {
                parentOfA.left = B;
            }
            else {
                parentOfA.right = B;
            }
        }

        A.right = B.left; // Make T2 the right subtree of A
        B.left = A;
        updateHeight(A);
        updateHeight(B);
    }

    private void balanceRL(AVLTreeNode<E> A, AVLTreeNode<E> parentOfA) {
        AVLTreeNode<E> B = A.right; // A is right-heavy
        AVLTreeNode<E> C = B.left; // B is left-heavy

        if (A == root) {
            root = C;
        }
        else {
            if (parentOfA.left == A) {
                parentOfA.left = C;
            }
            else {
                parentOfA.right = C;
            }
        }

        A.right = C.left; // Make T2 the right subtree of A
        B.left = C.right; // Make T3 the left subtree of B
        C.left = A;
        C.right = B;

        // Adjust heights
        updateHeight(A);
        updateHeight(B);
        updateHeight(C);
    }

    /**This is part of my attempts to get streaming working by cping from the Collection method*/
    public Stream<E> stream(){
        return StreamSupport.stream(spliterator(),false);
    }

    public Stream<E> parallelStream() {
        return StreamSupport.stream(spliterator(), true);
    }

    /** Delete an element from the binary tree.
     * Return true if the element is deleted successfully
     * Return false if the element is not in the tree */
    public boolean remove(E element) {
        if (root == null) {
            return false; // Element is not in the tree
        }

        // Locate the node to be deleted and also locate its parent node
        AVLTreeNode<E> parent = null;
        AVLTreeNode<E> current = root;
        while (current != null) {
            if (element.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            }
            else if (element.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            }
            else {
                break; // Element is in the tree pointed by current
            }
        }

        if (current == null)
            return false; // Element is not in the tree

        // Case 1: current has no left children (See Figure 23.6)
        if (current.left == null) {
            // Connect the parent with the right child of the current node
            if (parent == null) {
                root = current.right;
            }
            else {
                if (element.compareTo(parent.element) < 0)
                    parent.left = current.right;
                else
                    parent.right = current.right;

                // Balance the tree if necessary
                balancePath(parent.element);
            }
        }
        else {
            AVLTreeNode<E> parentOfRightMost = current;
            AVLTreeNode<E> rightMost = current.left;

            while (rightMost.right != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.right;
            }

            current.element = rightMost.element;

            if (parentOfRightMost.right == rightMost)
                parentOfRightMost.right = rightMost.left;
            else {
                parentOfRightMost.left = rightMost.left;
            }

            balancePath(parentOfRightMost.element);
        }

        size--;
        return true;
    }

    /**Below is the method I'd rather use, now a helper method*/
    public boolean contains(E e) {
        AVLTreeNode<E> current = root;

        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else {
                return true; // Element is found
            }
        }
        return false;
    }

    @SuppressWarnings("NullableProblems")
    public java.util.Iterator<E> iterator() {
        return new InOrderIterator();
    }

    public Object[] toArray() {
        InOrderIterator inOrderIterator = new InOrderIterator();
        return inOrderIterator.getList().toArray();
    }

    public void clear() {
        root = null;
        size = 0;
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder("{");
        toString(root, stringBuilder);
        stringBuilder.append("}");

        return stringBuilder.toString();
    }
    /**another helper method made in the mess of merging class. TODO inline later, or maybe have the toString call
     * the in order iterator*/
    public void toString(AVLTreeNode<E> node, StringBuilder stringBuilder){
        if (node == null){
            return;
        }

        if (node.left != null){
            toString(node.left, stringBuilder);
            stringBuilder.append(", ");
        }

        stringBuilder.append(node);

        if (node.right != null) {
            stringBuilder.append(", ");
            toString(node.right, stringBuilder);
        }
    }

    private static class AVLTreeNode<E extends Comparable<E>>  {
        E element;
        AVLTreeNode<E> left;
        AVLTreeNode<E> right;
        int height = 0; // New data field

        public AVLTreeNode(E e) {
            element = e;
        }

        public String toString(){
            return element.toString();
        }
    }

    /**instant this is constructed it iterates and stores all elements in a list, which is then the base of future
     * actions. I am not sure about this from an efficiency standpoint, would be able to iterate externally if
     * needed...Well for now this is it*/
    private class InOrderIterator implements java.util.Iterator<E> {
        private java.util.ArrayList<E> list = new java.util.ArrayList<>();
        private int current = 0; // Point to the current element in list

        public InOrderIterator() {
            inOrderIterate(); // Traverse binary tree and store elements in list
        }

        private void inOrderIterate() {
            inOrderIterate(root);
        }

        private void inOrderIterate(AVLTreeNode<E> root) {
            if (root == null) {
                return;
            }
            inOrderIterate(root.left);
            list.add(root.element);
            inOrderIterate(root.right);
        }

        @Override /** More elements for traversing? */
        public boolean hasNext() {
            return current < list.size();
        }

        @Override /** Get the current element and move to the next */
        public E next() {
            return list.get(current++);
        }

        @Override /** Remove current element */
        public void remove() {
            AVLTree.this.remove(list.get(current)); // Delete the current element
            list.clear(); // Clear the list
            inOrderIterate(); // Rebuild the list
        }

        public ArrayList<E> getList() {
            return list;
        }
    }
}