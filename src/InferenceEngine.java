import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lucas ouellette-falkenstein,
 * PArt of Rolly expert system, for professor frank seidel
 * assignment 5
 */
public class InferenceEngine extends Application{
    private TextArea questionField = new TextArea();
    private TextArea answerField = new TextArea();
    private Button answerButton = new Button("check answer");
    private MultiLinkedList<String> knowledgeBase = new MultiLinkedList<>();

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        gridPane.add(new Label("question"), 0, 0);
        gridPane.add(questionField, 1 , 0);
        gridPane.add(new Label("answer"), 0, 1);
        gridPane.add(answerField, 1, 1);
        gridPane.add(answerButton, 1, 5);

        gridPane.setAlignment(Pos.CENTER);
        answerField.setEditable(false);
        GridPane.setHalignment(answerButton, HPos.RIGHT);

        answerButton.setOnAction(e -> query());

        Scene scene = new Scene(gridPane, 900, 900);
        primaryStage.setTitle("Rolly");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private void query() {//searches for term and sets it as the answer

        /**Below is the old main method stuff, here only for demo purposes todo remove this when rewriting*/
        String[] lee = {"SOUTH","DAVIS","VIRGINIA"};
        String[] davis = {"SOUTH","LEE","VIRGINIA","CONFEDERACY"};
        knowledgeBase.add("LEE", Arrays.asList(lee));
        knowledgeBase.add("DAVIS", Arrays.asList(davis));
        //end demo items

        String question = questionField.getText()
                                       .toUpperCase();
        /**we flip to upper case to keep consistent case for equality*/

        List<String> tokens = Arrays.asList(question.split("WHO |WAS |THE |OF |'?' |\\s+"));

        List<String> newtokens = tokens.parallelStream()//remove all whitespaces from tokens and list
                                         .filter(item -> !item.trim()
                                                              .isEmpty())
                       .map(String::trim)
                       .collect(Collectors.toList());

        List<String> answers = knowledgeBase.getNodes()//find all elements that match query term in token list
                                            .parallelStream()
                                            .map(item -> item.element)
                                            .filter(newtokens::contains)
                                            .collect(Collectors.toList());

        List<String> associationAnswers = knowledgeBase.getNodes() //get all elements that have associations with query
                                                  .parallelStream()
                                                  .filter(node -> !Collections.disjoint(newtokens,
                                                                                        node.associationsToType()))
                                                  .map(node -> node.element)
                                                  .collect(Collectors.toList());

        answers.addAll(associationAnswers);//todo find more concise way to merge above 2 lists

        answerField.setText("Possible values include " + answers + "\n Right now I am too stupid to tell you " +
                                    "more");

    }
}
