# README #
(Oct 2015) Rolly was created before I was familiar with Adjacency lists, proper algorithmic efficiency, and other things. Much of it ended up being me implementing things I had never heard of. Most of the code here is in the strictest sense really bad. But I came up with some clever-ish ideas on my own, and so I've left the code as it was, bad comments and all. (/Oct 2015) 

#######################

Rolly is a primitive expert system written to explore the functional programming concepts introduced in java 8. An exercise in lambdas if you will.
Some might say it currently is buggy and troubled, with concurrency/thread safety issues left and right, certain inner loops reaching O(n^3) time, and is generally ugly because frankly Java is not ideal for this.

In response:


    1. Of course it's buggy, it's pre-alpha by someone wading into unfamiliar territory.


    2. Logic first, optimize later, something something Donald Knuth


    3. This is also a class project and must be in Java. Come May-ish I will sit down and rewrite this in either Clojure (really get into functional concepts, good    concurrency), D (Can work at a higher level but sometimes drop down to lower level) Kotlin (like Java but prettier) or some other combination. 


To attempt to run: git clone https://LuFalkes@bitbucket.org/LuFalkes/rolly.git and attempt to run InferenceEngine as your main class. Nothing much to it now.